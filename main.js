var socket = io.connect("http://battletag.herokuapp.com/",{
    transports: ['websocket']
});
var kit=0
var cx=0
var cy=0
var kits=["knight","cowboy","elemental","ninja"]
var kstime=[0,0,0,0]
var stime=[0,0]
var selection=0
var attack=false
var data={queue1:[],queue3:[],games:[{players:[{it:false,itcd:0,tx:100,ty:100,x:100,y:100,c:[255,255,0],id:"test",cd:[4,16,64],mcd:[1,3,8],unlock:[true,true,true]}]}]}
var imgs={}
var screen=title
window.onbeforeunload = function(){
   socket.emit("quit",{});
}
function hasBuff(player,name){
	for(var i=0;i<player.buffs.length;i++)if(player.buffs[i].name==name)return true
	return false
}
function getDuration(player,name){
	for(var i=0;i<player.buffs.length;i++)if(player.buffs[i].name==name)return player.buffs[i].duration
	return 0
}
function setup(){

  frameRate(100)
	createCanvas(700,700)
  textFont(loadFont("assets\\pixel.ttf"))
  var imgnames=["arena","it","bullet","bomb","fire","shuriken"]

  for(var i=0;i<kits.length;i++){
    imgs[kits[i]]=loadImage("assets\\"+kits[i]+".png")
    imgs[kits[i]+"_weapon"]=loadImage("assets\\"+kits[i]+"_weapon.png")
    imgs[kits[i]+"_0"]=loadImage("assets\\"+kits[i]+"_0.png")
    imgs[kits[i]+"_1"]=loadImage("assets\\"+kits[i]+"_1.png")
    imgs[kits[i]+"_2"]=loadImage("assets\\"+kits[i]+"_2.png")
  }
  for(var i=0;i<imgnames.length;i++){
    imgs[imgnames[i]]=loadImage("assets\\"+imgnames[i]+".png")
  }
  socket.on('heartbeat',function(d){
	   data=d
  }).on('death',function(data){
    if(data.id==socket.id)screen=dead
  }).on('win',function(data){
    if(data.id==socket.id)screen=win
  })
}
function mousePressed(){
  if(screen==info)screen=title
	if(screen==game)attack=true
  if(screen==title&&stime[0]>0.5){
    socket.emit("join",{mode:"1v1",kit})
    screen=game
  }
  if(screen==title&&stime[1]>0.5){
    socket.emit("join",{mode:"3v3",kit})
    screen=game
  }
  for(var i=0;i<kits.length;i++)if(screen==title&&(kstime[i]>.5)){
    kit=i
    kstime[i]=0
    screen=info
  }
  if(screen==win)screen=title
  if(screen==lose)screen=title
}
function keyPressed(){
  if(screen==game){
    if(key=="q")socket.emit("ability",{type:0})
    if(key=="w")socket.emit("ability",{type:1})
    if(key=="e")socket.emit("ability",{type:2})
  }
}
function draw(){
  screen()
}
function info(){
  background(100)
  if(kit==0){
    image(imgs.knight,50,150,300,300)
    textSize(100)
    fill(0)
    text("Knight",width/4-textWidth("Knight")/2,100)
    image(imgs.knight_0,400,150,100,100)
    image(imgs.knight_1,400,300,100,100)
    image(imgs.knight_2,400,450,100,100)
    textSize(45)
    text("55 HP",520,60)
    text("Q - Dash",420,140)
    text("W - Protect",420,290)
    text("E - Heal",420,440)
    textSize(15)
    text("Cooldown: 2s\n\nDash foward",510,170)
    text("Cooldown: 4s\n\nPushes nearby\nplayers away",510,320)
    text("Cooldown: 8s\n\nHeal 4 seconds of HP",510,470)
  }
  if(kit==1){
    image(imgs.cowboy,50,150,300,300)
    textSize(100)
    fill(0)
    text("Cowboy",width/4-textWidth("Knight")/2,100)
    image(imgs.cowboy_0,400,150,100,100)
    image(imgs.cowboy_1,400,300,100,100)
    image(imgs.cowboy_2,400,450,100,100)
    textSize(45)
    text("40 HP",520,60)
    text("Q - Pistol",420,140)
    text("W - Bomb",420,290)
    text("E - Haste",420,440)
    textSize(15)
    text("Cooldown: 1s\n\nShoot a projectile that\ncan tag enemies",510,170)
    text("Cooldown: 3s\n\nPlaces a bomb\nthat pushes players away\nafter a half second fuse",510,320)
    text("Cooldown: 10s\n\nGain a small permanent\nspeed boost",510,470)
  }
  if(kit==2){
    image(imgs.elemental,50,150,300,300)
    textSize(80)
    fill(0)
    text("Elemental",width/4-textWidth("Knight")/2,100)
    image(imgs.elemental_0,400,150,100,100)
    image(imgs.elemental_1,400,300,100,100)
    image(imgs.elemental_2,400,450,100,100)
    textSize(45)
    text("35 HP",520,60)
    text("Q - Fireball",420,140)
    text("W - Ingnite",420,290)
    text("E - Inferno",420,440)
    textSize(15)
    text("Cooldown: 1s\n\nShoot a projectile that\ndeals 2s of damage",510,170)
    text("Cooldown: 8s\n\nDash foward, creating\na trail of fire\n",510,320)
    text("Cooldown: 2s\n\nShoot a ring of fire\naround you",510,470)
  }
  if(kit==3){
    image(imgs.ninja,50,150,300,300)
    textSize(80)
    fill(0)
    text("Ninja",width/4-textWidth("Knight")/2,100)
    image(imgs.ninja_0,400,150,100,100)
    image(imgs.ninja_1,400,300,100,100)
    image(imgs.ninja_2,400,450,100,100)
    textSize(40)
    text("45 HP",520,60)
    text("Q - Backstab",420,140)
    text("W - Escape",420,290)
    text("E - Shuriken",420,440)
    textSize(15)
    text("Cooldown: 3s\n\nGain a 1 second\nspeed boost and lose\nrange for duration",510,170)
    text("Cooldown: 8s\n\nTurn Invisible\nfor 2 seconds",510,320)
    text("Cooldown: 5s\n\nShoot a shuriken that\nstuns enemies on hit",510,470)
  }
}

function win(){
  game()
  background(0,3)
  for(var i=0;i<256;i+=64){
    fill(255,255,i/2)
    textSize(150)
    text("You win!",width/2-textWidth("You win!")/2+sin(i+millis()/1000)*3+i/75,height/2)
  }
}
function lose(){
  game()
  background(0,3)
  for(var i=0;i<256;i+=64){
    fill(255,i/2,i/2)
    textSize(150)
    text("You lose.",width/2-textWidth("You lose.")/2+sin(i+millis()/1000)*3+i/75,height/2)
  }
}
function dead(){
  game()
  background(0,1)
  for(var i=0;i<256;i+=64){
    fill(255,255,i/2)
    textSize(100)
  }
}
function waiting(){
  background(0)
  for(var i=0;i<256;i+=64){
    fill(255,i,i/2)
    textSize(100)
    text("Waiting.",width/2-textWidth("Waiting...")/2+sin(i+millis()/1000)*5+i/75,height/2)
    if(millis()%4000>1000)text("Waiting.",width/2-textWidth("Waiting...")/2+sin(i+millis()/1000)*5+i/75,height/2)
    if(millis()%4000>2000)text("Waiting..",width/2-textWidth("Waiting...")/2+sin(i+millis()/1000)*5+i/75,height/2)
    if(millis()%4000>3000)text("Waiting...",width/2-textWidth("Waiting...")/2+sin(i+millis()/1000)*5+i/75,height/2)
    for(var j=0;j<data.queue1.length;j++)if(data.queue1[j].id==socket.id)text(data.queue1.length+"/2",width/2-textWidth(data.queue1.length+"/2")/2+sin(i+millis()/1000)*5+i/75,height*2/3)
    for(var j=0;j<data.queue3.length;j++)if(data.queue3[j].id==socket.id)text(data.queue3.length+"/6",width/2-textWidth(data.queue3.length+"/6")/2+sin(i+millis()/1000)*5+i/75,height*2/3)
  }
}
function title(){
  background(55)
  noSmooth()
  for(var i=0;i<256;i+=16){
    fill(255,i,i/2)
    textSize(100)
    text("BattleTag",125+sin(i+millis()/1000)*5+i/75,height/8+i/25+2*cos(i+millis()/1000))
    textSize(30*(2+stime[0]))
    fill(i/2,i*(stime[0]+1)/2,i/2)
    text("1v1 ("+data.queue1.length+" online)",width/2-textWidth("1v1 ("+data.queue1.length+" online)")/2+sin(i+(stime[0]+.1)*millis()/1000)*stime[0]*5+i/75,height*3/8+i/25+2*stime[0]*cos(i+(stime[0]+.1)*millis()/1000))
    textSize(30*(2+stime[1]))
    fill(i/2,i*(stime[1]+1)/2,i/2)
    text("3v3 ("+data.queue3.length+" online)",width/2-textWidth("3v3 ("+data.queue3.length+" online)")/2+sin(i+(stime[1]+.1)*millis()/1000)*stime[1]*5+i/75,height*5/8+i/25+2*stime[1]*cos(i+(stime[1]+.1)*millis()/1000))

  }
  if(Math.abs(height*3/8-mouseY)<height/16&&Math.abs(width/2-mouseX)<width/3)stime[0]+=.1
  if(Math.abs(height*5/8-mouseY)<height/16&&Math.abs(width/2-mouseX)<width/3)stime[1]+=.1
  if(Math.abs(height*3/8-mouseY)>height/16||Math.abs(width/2-mouseX)>width/3)stime[0]*=.1
  if(Math.abs(height*5/8-mouseY)>height/16||Math.abs(width/2-mouseX)>width/3)stime[1]*=.1
  for(var i=0;i<kits.length;i++){
    if(Math.abs(width*(i+1)/(kits.length+1)-mouseX)<32+4*kstime[0]&&Math.abs(height*3/4+64+4*kstime[0]-mouseY)<64+4*kstime[0]){
      kstime[i]+=.1
    }else{
      kstime[i]*=.1
    }
  }
  for(i=0;i<kits.length;i++)kstime[i]*=.9
  for(i=0;i<2;i++)stime[i]*=.9
  kstime[kit]+=1.5
  for(var i=0;i<kits.length;i++){
    tint(kstime[i]*50+205)
    image(imgs[kits[i]],width*(i+1)/(kits.length+1)-32-16*kstime[i],height*3/4-8*kstime[i],64+32*kstime[i],64+32*kstime[i])
  }
  kstime[kit]-=1.5
  }
function game(){
  noSmooth()
  noStroke();

  var ingame=false
	for(var i=0;i<data.games.length;i++)if(!data.games[i].over)for(var j=0;j<data.games[i].players.length;j++)if(data.games[i].players[j].id==socket.id){
		player=data.games[i].players[j]
    background(0);
    ingame=true
    if(mouseIsPressed)socket.emit("move",{x:player.x+mouseX-width/2-32,y:player.y+mouseY-height/2-32})
		if(attack){
			attack=false
			socket.emit("attack",{x:player.x+mouseX-width/2-32,y:player.y+mouseY-height/2-32})
		}


		push()
		cx*=.8
		cy*=.8
		cx+=.2*player.x
		cy+=.2*player.y
		translate(-cx+width/2-40,-cy+height/2-40)

    noTint()
		image(imgs.arena,-640,-640,1280,1280)
    fill(0,10)
    ellipse(player.x+40,player.y+40,player.range*2,player.range*2)
    for(var k=0;k<data.games[i].players.length;k++){
      noTint()
      var opponent=data.games[i].players[k]
      if(opponent==player||!hasBuff(opponent,"invisible")){
        if(hasBuff(opponent,"invisible"))tint(255,128)
        if(opponent.it)image(imgs.it,opponent.x+10,opponent.y-70-10*sin(millis()/500))
  			tint(255,200+opponent.ittime*10,200)
  			if(opponent.t==1)tint(200,200+opponent.ittime*10,255)
        fill(255)
        fill(255,5)
        if(hasBuff(opponent,"airblast"))for(var l=0;l<350-getDuration(opponent,"airblast")*1000;l+=10)ellipse(opponent.x+40,opponent.y+40,2*l,2*l)
  			image(imgs[opponent.kit],opponent.x,opponent.y,80,80)
        fill(0,255,0)
        rect(opponent.x-opponent.health+40,opponent.y-16,opponent.health*2,8)
        push()
        translate(opponent.x+40,opponent.y+40)
        rotate(-PI/2+atan2(opponent.ty-opponent.y,opponent.tx-opponent.x))
        image(imgs[opponent.kit+"_weapon"],-40,hasBuff(opponent,"dash")*(.3-getDuration(opponent,"dash"))*100,80,80)
        pop()
      }
		}
    for(var k=0;k<data.games[i].objects.length;k++){
      noTint()
      var obj=data.games[i].objects[k]
      if(obj.id=="bomb"&&obj.fuse<0){
        fill(255,5)
        for(var l=0;l<50-obj.fuse*1000;l+=10)ellipse(obj.x+40,obj.y+40,2*l,2*l)
      }else{
        image(imgs[obj.id],obj.x,obj.y,80,80)
      }
		}
    pop()
		fill(100)
    var hp=(Math.round(player.health*10)/10).toFixed(1)+"s"
    textSize(30)

		rect(155,height-120,370,120)

    fill(0,255,0)
    rect(width/2-player.health*5,height-150,player.health*10,32)
    fill(255)
    text(hp,width/2-textWidth(hp)/2,height-125)
		for(var l=0;l<3;l++){
			image(imgs[player.kit+"_"+l],180+l*110,height-112.5,105,105)
			fill(0,0,0,128)
			rect(180+l*110,height-112.5,105,player.cd[l]/[player.mcd,[5,20,50]][0+player.unlock[l]][l]*105)
			fill(255)
			textSize(30)
      cd=(Math.round(player.cd[l]*10)/10).toFixed(1)+"s"
      if(cd=="0.0s")cd="QWE"[l]
			text(cd,230+l*110-textWidth(cd)/2,height-55)
		}
	}
  if(!ingame&&screen==game)waiting()
  if(!ingame&&screen==dead)screen=lose
}
